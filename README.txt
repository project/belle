CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers
 

INTRODUCTION
---------------------

Belle is a responsive drupal 8 theme with many regions provided for advance
functionality. Along with many features of the theme one can change the entrire
look by configuring the theme.

Following are the featrues for the theme:

 * Change the entire look of the website by selecting the theme base color. 
 * Choose to make your site display in full width or with equal padding on both
   side with just one click.
 * Configurable responsive banner for the site.
 * Configurable table patterns.
 * Configurable image patterns.
 * No need to install any module for adding social links to the website.
   As belle comes with social links which are configurable from themes settings
   page.
 * Responsive.

 For a full description of the module, visit the project page:
   https://www.drupal.org/project/belle
   
 To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/belle


REQUIREMENTS
---------------------

No special requirements.


INSTALLATION
---------------------

Install as you would  normally install a Druapl contributed theme. 
Visit: https://www.drupal.org/docs/8/extending-drupal-8/installing-themes for
further informtaion.


CONFIGURATION
---------------------

Configure theme settings in Administration » Appearance » Settings » Belle:

Under Belle Advance Settings:

  * Click on General Settings:
    - For container
      * UnCheck for giving full width look to website.
      * Check for giving a centeralized look to website
    - For managing how tables should look.
      * Check "Table Striped" for adding zebra-stripes to a table.
      * Check "Table Bordered" for adding borders on all sides of the table and
        cells.
      * Check "Table Hover" for adding hover effect (grey background color) on 
        table rows.
      * Check "Table Condensed" for making tables more compact by cutting cell
        padding in half.
    - For managing how images should look.
      * Check "Responsive Image" for making your images automatically adjust to
        fit the size of the screen.
      * Check "Image Rounded" for adding rounded corners to an image (IE8 does
		not support rounded corners).
      * Check "Image Circle" shapes the image to a circle (IE8 does not support
		rounded corners).
      * Check "Image Thumbnail" shapes the image to a thumbnail.
    - For hiding hamburger on dektop only.
      * Toggle Hamburger and check the checkbox -  Hide Hamburger from Desktop.
    
  * Click on Banner:
    - Check "Enable banner" if want to show banner on pages.
    - Can configure to show banner on home page or on all pages.

  * Website Color: 
    - Select website base theme color.
  
  * Social Icons:
    - Enter the links of the social icons you want to show in the footer of you
      website.


MAINTAINERS
---------------------

Current maintainers:
  * Akansha Saxena : (https://www.drupal.org/u/saxenaakansha30)
