<?php

/**
 * @file
 * For altering settings form.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function belle_form_system_theme_settings_alter(&$form, FormStateInterface &$form_state, $form_id = NULL) {

  if (isset($form_id)) {
    return;
  }

  $form['belle_additional_settings'] = [
    '#type' => 'vertical_tabs',
    '#title' => t('<h2>Belle Advance Settings</h2>'),
  ];

  // For general settings..
  $form['general_settings'] = [
    '#type' => 'details',
    '#title' => t('General Settings'),
    '#group' => 'belle_additional_settings',
  ];

  // For container - giving website a full width or with equal padding look.
  $form['general_settings']['container'] = [
    '#type' => 'details',
    '#title' => t('Container'),
  ];

  $form['general_settings']['container']['active_container'] = [
    '#type' => 'checkbox',
    '#title' => t('Check to give some space on both side in websites.'),
    '#default_value' => theme_get_setting('active_container'),
    '#description' => t('This setting will give equal space on both side of 
     website. Uncheck to get container-fluid view.'),
  ];

  /* For table - for giving access to site builder for choosing respective table
  pattern.
   */
  $form['general_settings']['table_container'] = [
    '#type' => 'details',
    '#title' => t('Table'),
  ];

  $form['general_settings']['table_container']['table'] = [
    '#type' => 'checkboxes',
    '#title' => t('Table Classes'),
    '#default_value' => NULL !== theme_get_setting('image') ? theme_get_setting('table') : ['table-hover', 'table-bordered'],
    '#options' => [
      'table-striped' => t('Table Striped'),
      'table-bordered' => t('Table Bordered'),
      'table-hover' => t('Table Hover'),
      'table-condensed' => t('Table Condensed'),
    ],
    '#description' => t('The <b>.table-striped</b> class adds zebra-stripes to a
     table<br />The <b>.table-bordered</b> class add borders on all sides of the
     table and cells<br />The <b>.table-hover</b> class adds a hover effect 
     (grey background color) on table rows<br />The <b>.table-condensed</b> 
     class makes a table more compact by cutting cell padding in half'),
  ];

  /* For Responsive Images - For giving site builders a functionality to change
   * the look and feel of the images.
   */
  $form['general_settings']['image_container'] = [
    '#type' => 'details',
    '#title' => t('Images'),
  ];

  $form['general_settings']['image_container']['image_responsive'] = [
    '#type' => 'checkbox',
    '#title' => t('Responsive Image'),
    '#default_value' => theme_get_setting('image_responsive'),
    '#description' => t('Images come in all sizes. So do screens. Responsive 
    images automatically adjust to fit the size of the screen.<br />Create 
    responsive images by adding an <b>.img-responsive</b> class to the 
    <b><img></b> tag. The image will then scale nicely to the parent element.
    </br />The .img-responsive class applies <b>display: block;</b> and 
    <b>max-width: 100%;</b> and <b>height: auto;</b> to the image'),
  ];

  $form['general_settings']['image_container']['image'] = [
    '#type' => 'radios',
    '#title' => t('Image Style'),
    '#default_value' => NULL !== theme_get_setting('image') ? theme_get_setting('image') : 'img-thumbnail',
    '#options' => [
      'img-rounded' => t('Img Rounded'),
      'img-circle' => t('Img Circle'),
      'img-thumbnail' => t('Img Thumbnail'),
    ],
    '#description' => t('The <b>.img-rounded</b> class adds rounded corners to 
    an image (IE8 does not support rounded corners)<br />The <b>.img-circle</b> 
    class shapes the image to a circle (IE8 does not support rounded corners)
    <br />The <b>.img-thumbnail</b> class shapes the image to a thumbnail'),
  ];

  // Functionality for hiding hamburger menu on destop.
  $form['general_settings']['hamburger'] = [
    '#type' => 'details',
    '#title' => t('Hamburger'),
  ];

  $form['general_settings']['hamburger']['hamburger_display'] = [
    '#type' => 'checkbox',
    '#title' => t('Hide hamburger from desktops'),
    '#default_value' => theme_get_setting('hamburger_display'),
    '#description' => t('Check to hide the hamburger menu in desktop only'),
  ];

  // For banner - for configuring banner shown right below header.
  $form['banner'] = [
    '#type' => 'details',
    '#title' => t('Banner'),
    '#group' => 'belle_additional_settings',
  ];

  // Banner slide.
  $form['banner']['slide_state'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable banner'),
    '#description' => t('Check to show banner. If no image or caption is provided default will be used.'),
    '#default_value' => theme_get_setting('slide_state'),
  ];

  $form['banner']['slide'] = [
    '#type' => 'details',
    '#title' => t('Banner'),
    '#states' => [
      'invisible' => [
        ':input[name="slide_state"]' => ['checked' => FALSE],
      ],
    ],
    '#open' => TRUE,
  ];

  $form['banner']['slide']['show_default_image'] = [
    '#type' => 'checkbox',
    '#title' => t('Use the banner image supplied by the theme'),
    '#description' => t('Uncheck to upload your own image'),
    '#default_value' => theme_get_setting('show_default_image'),
  ];

  $form['banner']['slide']['image_parallax'] = [
    '#type' => 'checkbox',
    '#title' => t('Banner Image Parallax'),
    '#description' => t('Check to add parallax effect(scrolling of banner image).'),
    '#default_value' => theme_get_setting('image_parallax'),
  ];

  $form['banner']['slide']['coursel_image'] = [
    '#type' => 'managed_file',
    '#title' => t('Image'),
    '#description' => t('Ideal image size is 1304x316px.'),
    '#default_value' => theme_get_setting('coursel_image'),
    '#upload_location' => 'public://',
    '#states' => [
      'invisible' => [
        ':input[name="show_default_image"]' => ['checked' => TRUE],
      ],
    ],
  ];

  $form['banner']['slide']['coursel_caption_header'] = [
    '#type' => 'textfield',
    '#title' => t('Caption header text'),
    '#default_value' => theme_get_setting('coursel_caption_header'),
  ];

  $form['banner']['slide']['coursel_caption_body'] = [
    '#type' => 'textarea',
    '#title' => t('Caption body text'),
    '#default_value' => theme_get_setting('coursel_caption_body'),
  ];

  $form['banner']['slide']['display_on_pages'] = [
    '#type' => 'radios',
    '#title' => t('Show banner on pages'),
    '#default_value' => NULL !== theme_get_setting('display_on_pages') ? theme_get_setting('display_on_pages') : 'front',
    '#options' => [
      'front' => t('Home/Front Page'),
      'all' => t('All Pages'),
    ],
    '#description' => t('Choose where the banner should be shown.'),
  ];

  // Website Makeover - change the theme base color.
  $form['website_color'] = [
    '#type' => 'details',
    '#title' => t('Website Color'),
    '#group' => 'belle_additional_settings',
  ];

  $form['website_color']['website_base_color'] = [
    '#type' => 'radios',
    '#title' => t('Select website theme color'),
    '#default_value' => NULL !== theme_get_setting('website_base_color') ? theme_get_setting('website_base_color') : 'sea-blue',
    '#options' => [
      'carrot-red' => t('Carrot Red'),
      'historical-brown' => t('Historical Brown'),
      'parrot-green' => t('Parrot Green'),
      'professional-blue' => t('Professional Blue'),
      'purple' => t('Purple'),
      'sea-blue' => t('Sea Blue'),
    ],
    '#description' => t('Select the base colour of the theme to give a complete 
    new look to the website.s'),
  ];

  // Social Icons - social icons shown in footer.
  $form['social_icons'] = [
    '#type' => 'details',
    '#title' => t('Social Icons'),
    '#group' => 'belle_additional_settings',
  ];

  $form['social_icons']['facebook_link'] = [
    '#type' => 'url',
    '#title' => t('Facebook link'),
    '#default_value' => theme_get_setting('facebook_link'),
  ];

  $form['social_icons']['youtube_link'] = [
    '#type' => 'url',
    '#title' => t('Youtube link'),
    '#default_value' => theme_get_setting('youtube_link'),
  ];

  $form['social_icons']['twitter_link'] = [
    '#type' => 'url',
    '#title' => t('Twitter link'),
    '#default_value' => theme_get_setting('twitter_link'),
  ];

  $form['social_icons']['google_plus_link'] = [
    '#type' => 'url',
    '#title' => t('Google Plus link'),
    '#default_value' => theme_get_setting('google_plus_link'),
  ];

  $form['social_icons']['linkedin_link'] = [
    '#type' => 'url',
    '#title' => t('Linedin link'),
    '#default_value' => theme_get_setting('linkedin_link'),
  ];

  $form['social_icons']['drupal_link'] = [
    '#type' => 'url',
    '#title' => t('Drupal Profile link'),
    '#default_value' => theme_get_setting('drupal_link'),
  ];

}
