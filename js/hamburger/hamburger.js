/**
 * @file
 * js template for hamburger menu.
 */

// Hamburger toggle functionality.
jQuery('#show-hamburger').click(function () {
  'use strict';
  jQuery('#hamburger').toggle('linear');
});

jQuery('.hamburger-collapse-outer-wrappers .close-navigation .fa').click(function () {
  'use strict';
  jQuery('#hamburger').toggle('linear');
});
